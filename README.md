# Bedrijventerrein

Collection of datasets about business areas / bedrijventerreinen.

Our data sources:

 - Top10nl
 - IBIS bedrijventerreinen https://data.overheid.nl/dataset/ibis-bedrijventerreinen
 - eventuele andere bronnen.
